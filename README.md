# Pagination lib

Library to generate pagination

## I. Required
jQuery, Bootstrap, fontawesome

## II. How to use

```javascript
$("#my-div").paginator({
    "currentPage": null,            // current page, set to null for first page
    "total": 60,                    // total numbers of rows
    "pageInterval": 7,              // page interval to show in paginator
    "className": "btn btn-default", // class to set style of button of the pagination
    "baseUrl": "path-list"          // url of each page without the page number
});
```

## III. Starting projet
Clone or Dowload project and open **index.html**
