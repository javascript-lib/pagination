var currentPage = 1;
var totalPage = 10;
var className = "";
var baseUrl = "";

var interval = 10;

var start = 1;
var end = 0;

jQuery.fn.paginator = function(data) {
	currentPage = data.currentPage ? data.currentPage : 1;
	totalPage = data.total;
	interval = data.pageInterval;
	className = data.className;
	baseUrl = data.baseUrl;

	if(currentPage) {
		start = currentPage % interval == 0 ? currentPage - interval + 1 : (currentPage - (currentPage % interval)) + 1;
	}

	this.empty();
	
	this.append('<a href="javascript:void(0)" id="pagination-left" class="' + className + '"><i class="fa fa-arrow-left"></i></a>');
	this.append('<div class="pagination"></div>');
	this.append('<a href="javascript:void(0)" id="pagination-right" class="' + className + '"><i class="fa fa-arrow-right"></i></a>');
	this.append('<strong> sur ' + totalPage + '</strong>');
	
	setPaginator();

	$("#pagination-left").click(function() {
		start = start - interval;
		setPaginator();
	});

	$("#pagination-right").click(function() {
		start = end + 1;
		setPaginator();
	});
}

function setPaginator() {
	setEnd();
	checkSartPage();
	checkLastPage(totalPage);
	generatePagination(start, end, className);
}

function generatePagination(start, end, className) {
	$(".pagination").empty();
	for(var i = start; i <= end; i++) {
		if(currentPage && currentPage == i) {
			let activePage = className + " active";
			$(".pagination").append('<a href="' + baseUrl + '/' + i + '" class="' + activePage + '" style="margin:2.5px;">' + i + '</a>');
		} else {
			$(".pagination").append('<a href="' + baseUrl + '/' + i + '" class="' + className + '" style="margin:2.5px;">' + i + '</a>');
		}
	}
}

function checkLastPage(totalPage) {
	if(totalPage <= end) {
		end = totalPage;
		$("#pagination-right").hide();
	} else {
		$("#pagination-right").show();
	}
}

function setEnd() {
	end = start + interval - 1;
}

function checkSartPage() {
	if(start == 1){
		$("#pagination-left").hide();
	} else {
		$("#pagination-left").show();
	}
}